const listaPaises = document.querySelector("#listaPaises");
const buscarInput = document.querySelector("#buscar");
const selectRegion = document.querySelector("#region");
let URL = "https://restcountries.com/v3.1/";

function mostrarPais(pais) {
  let population = pais.population.toLocaleString("es-MX");

  const countryDiv = document.createElement("div");
  countryDiv.classList.add("country");
  countryDiv.innerHTML = `
    <div class="country-img">
      <img src="${pais.flags.png}" alt="${pais.name.common}" />
    </div>
    <div class="country-info">
      <h2 class="name">${pais.name.common}</h2>
      <div class="datos">
        <p class="dato"><strong>Population: </strong>${population}</p>
        <p><strong>Region: </strong>${pais.region}</p>
        <p><strong>Capital: </strong>${pais.capital}</p>
      </div>
    </div>
  `;
  listaPaises.append(countryDiv);
}

const mostrarTodos = () => {
  fetch(URL + "all")
    .then((response) => response.json())
    .then((data) => {
      console.log(data.slice(0, 100));
      data.slice(0, 100).forEach((pais) => mostrarPais(pais));
    });
};

buscarInput.addEventListener("search", (e) => {
  let nameABuscar = e.currentTarget.value;
  listaPaises.innerHTML = "";
  fetch(URL + "name/" + nameABuscar)
    .then((response) => response.json())
    .then((data) => {
      mostrarPais(data[0]);
    });
});

selectRegion.addEventListener("change", (e) => {
  let regionABuscar = e.currentTarget.value;
  listaPaises.innerHTML = "";
  if (regionABuscar === "all") mostrarTodos();
  else {
    fetch(URL + "region/" + regionABuscar)
      .then((response) => response.json())
      .then((data) => {
        data.forEach((pais) => mostrarPais(pais));
      });
  }
});

mostrarTodos();
